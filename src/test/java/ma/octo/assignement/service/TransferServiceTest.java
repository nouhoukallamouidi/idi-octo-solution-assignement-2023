package ma.octo.assignement.service;

import static ma.octo.assignement.globalconstants.constants.MONTANT_TRANSFER_MAXIMAL;
import static ma.octo.assignement.globalconstants.constants.MONTANT_TRANSFER_MINIMAL;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

@SpringBootTest
class TransferServiceTest {

	@Autowired
	private TransferService transferService;

	@Test
	void testIfEmetteurNotExists() {
		Compte beneficiaire = new Compte();
		beneficiaire.setSolde(BigDecimal.valueOf(1000));
		String nrBeneficiaire = "Nr1";
		beneficiaire.setNrCompte(nrBeneficiaire);
		beneficiaire.setRib("RIB1");

		String NrEmetteurNonExistant = "NrEmetteur";

		assertThrows(CompteNonExistantException.class,
				() -> transferService.verifyEmetteurAndBeneficiare(NrEmetteurNonExistant, nrBeneficiaire));
	}

	@Test
	void testIfBeneficiaireNotExists() {
		Compte beneficiaire = new Compte();
		beneficiaire.setSolde(BigDecimal.valueOf(1000));
		String nrEmetteur = "Nr1";
		beneficiaire.setNrCompte(nrEmetteur);
		beneficiaire.setRib("RIB1");

		String NrBeneficiareNonExistant = "NrBeneficiaire";

		assertThrows(CompteNonExistantException.class,
				() -> transferService.verifyEmetteurAndBeneficiare(NrBeneficiareNonExistant, nrEmetteur));
	}

	@Test
	void testIfEmetteurIsBeneficiaire() {
		assertThrows(TransactionException.class, () -> transferService.verifyEmetteurAndBeneficiare("100", "100"));
	}

	@Test
	void testIfMontantIsNull() {
		assertThrows(TransactionException.class, () -> transferService.verifyMontant(null));
	}

	@Test
	void testIfMontantIsZero() {
		assertThrows(TransactionException.class, () -> transferService.verifyMontant(BigDecimal.ZERO));
	}

	@Test
	void testIfMontantIsBiggerThanMaximal() {
		assertThrows(TransactionException.class,
				() -> transferService.verifyMontant(BigDecimal.valueOf(MONTANT_TRANSFER_MAXIMAL + 2)));
	}

	@Test
	void testIfMontantIsSmallerThanMinimal() {
		assertThrows(TransactionException.class,
				() -> transferService.verifyMontant(BigDecimal.valueOf(MONTANT_TRANSFER_MINIMAL - 1)));
	}

	@Test
	void testIfMotifIsNull() {
		assertThrows(TransactionException.class, () -> transferService.verifyMotif(null));
	}

	@Test
	void testIfMotifIsEmpty() {
		assertThrows(TransactionException.class, () -> transferService.verifyMotif(""));
	}

	@Test
	void verifyEmetteurSolde() {

		assertThrows(SoldeDisponibleInsuffisantException.class,
				() -> transferService.verifyEmetteurSolde(BigDecimal.ONE, BigDecimal.TEN));
	}
}