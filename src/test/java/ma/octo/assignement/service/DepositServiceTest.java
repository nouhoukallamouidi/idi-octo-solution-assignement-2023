package ma.octo.assignement.service;

import static ma.octo.assignement.globalconstants.constants.MONTANT_DEPOSIT_MAXIMAL;
import static ma.octo.assignement.globalconstants.constants.MONTANT_DEPOSIT_MINIMAL;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

@SpringBootTest
class DepositServiceTest {
	@Autowired
	private DepositService depositService;

	@Test
	void testIfDepositsEmpty() {
		assertThat(depositService.getDeposits()).isEqualTo(ResponseEntity.notFound().build());
	}

	@Test
	void testIfCompteNotExists() {
		assertThrows(CompteNonExistantException.class, () -> depositService.VerifyBeneficiaire("RIB", null));
	}

	@Test
	void testIfMontantIsNull() {
		assertThrows(TransactionException.class, () -> depositService.verifyMontant(null));
	}

	@Test
	void testIfMontantIsZero() {
		assertThrows(TransactionException.class, () -> depositService.verifyMontant(BigDecimal.ZERO));
	}

	@Test
	void testIfMontantIsBiggerThanMaximal() {
		assertThrows(TransactionException.class,
				() -> depositService.verifyMontant(BigDecimal.valueOf(MONTANT_DEPOSIT_MAXIMAL + 2)));
	}

	@Test
	void testIfMontantIsSmallerThanMinimal() {
		assertThrows(TransactionException.class,
				() -> depositService.verifyMontant(BigDecimal.valueOf(MONTANT_DEPOSIT_MINIMAL - 1)));
	}

	@Test
	void testIfMotifIsNull() {
		assertThrows(TransactionException.class, () -> depositService.verifyMotif(null));
	}

	@Test
	void testIfMotifIsEmpty() {
		assertThrows(TransactionException.class, () -> depositService.verifyMotif(""));
	}

	@Test
	void testIfNomPrenomEmetteurisNull() {
		assertThrows(TransactionException.class, () -> depositService.verifyNomPrenomEmetteur(null));
	}

	@Test
	void testIfNomPrenomEmetteurisEmpty() {
		assertThrows(TransactionException.class, () -> depositService.verifyNomPrenomEmetteur(""));
	}

}