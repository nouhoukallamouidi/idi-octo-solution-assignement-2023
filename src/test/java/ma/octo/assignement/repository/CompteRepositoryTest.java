package ma.octo.assignement.repository;

import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.domain.model.Utilisateur;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class CompteRepositoryTest {

	@Autowired
	private CompteRepository compteRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Test
	void trouveCompteByNrCompte() {
		Compte compte1 = new Compte();
		String nrCompte1 = "1012000122222";
		compte1.setNrCompte(nrCompte1);

		Compte compte2 = new Compte();
		String nrCompte2 = "100992330";
		compte2.setNrCompte(nrCompte2);

		compteRepository.saveAll(List.of(compte1, compte2));

		assertThat(compteRepository.findByNrCompte(nrCompte1)).isEqualTo(compte1);
	}

	 @Test
	void TrouveCompteByRib() {
		String rib = "RIB";
		Compte compte = new Compte();
		compte.setRib(rib);

		compteRepository.save(compte);

		Compte CompteTrouve = compteRepository.findByRib(rib);

		assertThat(CompteTrouve).isEqualTo(compte);
	}

	@Test
	void TrouveComptesByUtilisateurId() {
		Compte compte1 = new Compte();
		compte1.setNrCompte("110000110");

		Compte compte2 = new Compte();
		compte2.setNrCompte("110002220");

		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setFirstname("first");
		utilisateur.setLastname("Last");
		utilisateur.setGender("Male");
		utilisateur.setUsername("user");

		compte1.setUtilisateur(utilisateur);
		compte2.setUtilisateur(utilisateur);

		Long utilisateurId = utilisateurRepository.save(utilisateur).getId();

		compteRepository.saveAll(List.of(compte1, compte2));

		List<Compte> ComptesTrouve = compteRepository.findComptesByUtilisateurId(utilisateurId);
		assertThat(ComptesTrouve).isEqualTo(List.of(compte1, compte2));
	}

}