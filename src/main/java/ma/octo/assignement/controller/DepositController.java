package ma.octo.assignement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.model.Deposit;
import ma.octo.assignement.dto.model.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;

@RestController
@RequestMapping(value = "api/deposits")
public class DepositController {

    private final DepositService depositService;

    @Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @GetMapping
    ResponseEntity<List<Deposit>> getDeposits() {
        return depositService.getDeposits();
    }

    @PostMapping
    ResponseEntity<Deposit> createDeposit(@RequestBody DepositDto depositDto) throws TransactionException, CompteNonExistantException {
        return depositService.createDeposit(depositDto);
    }
}
