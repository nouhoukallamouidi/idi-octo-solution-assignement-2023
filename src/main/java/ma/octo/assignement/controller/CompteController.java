package ma.octo.assignement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.service.CompteService;

@RestController
@RequestMapping(value = "api/comptes")
public class CompteController {

	private final CompteService compteService;

	@Autowired
	public CompteController(CompteService compteService) {
		this.compteService = compteService;
	}

	@GetMapping
	ResponseEntity<List<Compte>> getComptes() {
		return compteService.getComptes();
	}
}