package ma.octo.assignement.service;

import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.domain.model.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Service
public class UtilisateurService {

	private final UtilisateurRepository utilisateurRepository;
	private final CompteRepository compteRepository;

	Logger LOGGER = LoggerFactory.getLogger(UtilisateurService.class);

	@Autowired
	public UtilisateurService(UtilisateurRepository utilisateurRepository, CompteRepository compteRepository) {
		this.utilisateurRepository = utilisateurRepository;
		this.compteRepository = compteRepository;
	}

	public ResponseEntity<List<Utilisateur>> getUtilisateurs() {

		LOGGER.info("Listes des utilisateurs");
		List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
		if (utilisateurs.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(utilisateurs);
		}
	}

	public ResponseEntity<List<Compte>> getUtilisateurComptes(Long utilisateurId) {

		LOGGER.info("Listes des Comptes d'un utilisateur");

		List<Compte> comptes = compteRepository.findComptesByUtilisateurId(utilisateurId);
		if (comptes.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(comptes);
		}
	}
}
