package ma.octo.assignement.service;

import static ma.octo.assignement.globalconstants.constants.MONTANT_TRANSFER_MAXIMAL;
import static ma.octo.assignement.globalconstants.constants.MONTANT_TRANSFER_MINIMAL;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.domain.model.Transfer;
import ma.octo.assignement.dto.model.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;

@Service
@Slf4j
public class TransferService {

	private final AuditService auditService;

	private final TransferRepository transferRepository;

	private final CompteRepository compteRepository;

	Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

	public TransferService(AuditService auditService, TransferRepository transferRepository,
			CompteRepository compteRepository) {
		this.auditService = auditService;
		this.transferRepository = transferRepository;
		this.compteRepository = compteRepository;
	}

	public ResponseEntity<List<Transfer>> getTransfers() {

		LOGGER.info("Lister les transfers");
		var transfers = transferRepository.findAll();

		if (transfers.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(transfers);
		}
	}

	public ResponseEntity<Transfer> createTransfer(TransferDto transferDto)
			throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

		LOGGER.info("Creation d'un nouveau tranfer");

		verifyTransferDto(transferDto);
		updateSolde(transferDto);
		Transfer transfer = saveTransfer(transferDto);

		return new ResponseEntity<Transfer>(transfer, HttpStatus.CREATED);
	}

	private void verifyTransferDto(TransferDto transferDto)
			throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
		String nrEmetteur = transferDto.getNrCompteEmetteur();
		String nrBeneficiaire = transferDto.getNrCompteBeneficiaire();
		BigDecimal montant = transferDto.getMontant();
		String motif = transferDto.getMotif();
		Date date = transferDto.getDate();

		BigDecimal emetteurSolde = compteRepository.findByNrCompte(nrEmetteur).getSolde();
		BigDecimal beneficiaireMontant = transferDto.getMontant();

		verifyEmetteurAndBeneficiare(nrEmetteur, nrBeneficiaire);
		verifyMontant(montant);
		verifyMotif(motif);
		verifyEmetteurSolde(emetteurSolde, beneficiaireMontant);
		verifyDate(transferDto, date);
	}

	public void verifyEmetteurAndBeneficiare(String nrEmetteur, String nrBeneficiaire)
			throws TransactionException, CompteNonExistantException {
		if (nrEmetteur.equals(nrBeneficiaire)) {
			throw new TransactionException(
					"Un Compte ne peut pas transférer de l'argent vers lui-même, le transfer se fait d'un compte à un autre");
		}

		Compte emetteur = compteRepository.findByNrCompte(nrEmetteur);
		Compte beneficiaire = compteRepository.findByNrCompte(nrBeneficiaire);

		if (emetteur == null) {
			throw new CompteNonExistantException("Compte avec numero " + nrEmetteur + " est introuvable");
		}

		if (beneficiaire == null) {
			throw new CompteNonExistantException("Compte avec numero " + nrBeneficiaire + " est introuvable");
		}
	}

	public void verifyMontant(BigDecimal montant) throws TransactionException {
		if (montant == null || montant.intValue() == 0) {
			throw new TransactionException("Montant vide");
		} else if (montant.intValue() < MONTANT_TRANSFER_MINIMAL) {
			throw new TransactionException("Montant minimal de transfer non atteint");
		} else if (montant.intValue() > MONTANT_TRANSFER_MAXIMAL) {
			throw new TransactionException("Montant maximal de transfer dépassé");
		}
	}

	public void verifyMotif(String motif) throws TransactionException {
		if (motif == null || motif.isEmpty()) {
			throw new TransactionException("Motif vide");
		} else if (motif.length() > 250) {
			throw new TransactionException("Motif est trop longue");
		}
	}

	public void verifyEmetteurSolde(BigDecimal emetteurSolde, BigDecimal beneficiaireMontant)
			throws SoldeDisponibleInsuffisantException {
		if (emetteurSolde.intValue() - beneficiaireMontant.intValue() < 0) {
			throw new SoldeDisponibleInsuffisantException("Solde disponible insuffisant");
		}
	}

	public void verifyDate(TransferDto transferDto, Date date) {
		if (date == null) {
			transferDto.setDate(new Date());
		}
	}

	private void updateSolde(TransferDto transferDto) {
		String nrEmetteur = transferDto.getNrCompteEmetteur();
		String nrBeneficiaire = transferDto.getNrCompteBeneficiaire();

		Compte emetteur = compteRepository.findByNrCompte(nrEmetteur);
		Compte beneficiaire = compteRepository.findByNrCompte(nrBeneficiaire);

		emetteur.setSolde(emetteur.getSolde().subtract(transferDto.getMontant()));
		compteRepository.save(emetteur);

		beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
		compteRepository.save(beneficiaire);
	}

	private Transfer saveTransfer(TransferDto transferDto) {
		String nrEmetteur = transferDto.getNrCompteEmetteur();
		String nrBeneficiaire = transferDto.getNrCompteBeneficiaire();

		Compte emetteur = compteRepository.findByNrCompte(nrEmetteur);
		Compte beneficiaire = compteRepository.findByNrCompte(nrBeneficiaire);

		Transfer transfer = new Transfer();
		transfer.setDateExecution(transferDto.getDate());
		transfer.setCompteBeneficiaire(beneficiaire);
		transfer.setCompteEmetteur(emetteur);
		transfer.setMontantTransfer(transferDto.getMontant());
		transfer.setMotifTransfer(transferDto.getMotif());

		auditService.auditTransfer(transferDto);
		return transferRepository.save(transfer);
	}
}
