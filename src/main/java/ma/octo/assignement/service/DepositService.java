package ma.octo.assignement.service;

import static ma.octo.assignement.globalconstants.constants.MONTANT_DEPOSIT_MAXIMAL;
import static ma.octo.assignement.globalconstants.constants.MONTANT_DEPOSIT_MINIMAL;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.model.Compte;
import ma.octo.assignement.domain.model.Deposit;
import ma.octo.assignement.dto.model.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;

@Service
public class DepositService {

	private final DepositRepository depositRepository;
	private final CompteRepository compteRepository;
	private final AuditService auditService;

	@Autowired
	public DepositService(DepositRepository depositRepository, CompteRepository compteRepository,
			AuditService auditService) {
		this.depositRepository = depositRepository;
		this.compteRepository = compteRepository;
		this.auditService = auditService;
	}

	public ResponseEntity<List<Deposit>> getDeposits() {
		List<Deposit> deposits = depositRepository.findAll();

		if (deposits.isEmpty()) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(deposits);
		}
	}

	public ResponseEntity<Deposit> createDeposit(DepositDto depositDto)
			throws TransactionException, CompteNonExistantException {
		verifyDepositDto(depositDto);
		updateSolde(depositDto);
		Deposit deposit = saveDeposit(depositDto);
		return new ResponseEntity<Deposit>(deposit, HttpStatus.CREATED);
	}

	public void verifyDepositDto(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
		String rib = depositDto.getRib();
		Compte beneficiaire = compteRepository.findByRib(rib);
		BigDecimal montant = depositDto.getMontant();
		String motif = depositDto.getMotif();
		String nomPrenomEmetteur = depositDto.getNomPrenomEmetteur();
		Date date = depositDto.getDate();

		VerifyBeneficiaire(rib, beneficiaire);
		verifyMontant(montant);
		verifyMotif(motif);
		verifyNomPrenomEmetteur(nomPrenomEmetteur);
		verifyDate(depositDto, date);
	}

	public void VerifyBeneficiaire(String rib, Compte beneficiaire) throws CompteNonExistantException {
		if (beneficiaire == null) {
			throw new CompteNonExistantException("Compte avec rib " + rib + " est introuvable");
		}
	}

	public void verifyMontant(BigDecimal montant) throws TransactionException {
		if (montant == null || montant.intValue() == 0) {
			throw new TransactionException("Montant vide");
		} else if (montant.intValue() < MONTANT_DEPOSIT_MINIMAL) {
			throw new TransactionException("Montant minimal de transfer non atteint");
		} else if (montant.intValue() > MONTANT_DEPOSIT_MAXIMAL) {
			throw new TransactionException("Montant maximal de transfer dépassé");
		}
	}

	public void verifyMotif(String motif) throws TransactionException {
		if (motif == null || motif.isEmpty()) {
			throw new TransactionException("Motif vide");
		} else if (motif.length() > 200) {
			throw new TransactionException("Motif est trop longue");
		}
	}

	public void verifyNomPrenomEmetteur(String nomPrenomEmetteur) throws TransactionException {
		if (nomPrenomEmetteur == null || nomPrenomEmetteur.isEmpty()) {
			throw new TransactionException("Nom et Prénom de l'emetteur sont vides");
		}
	}

	public void verifyDate(DepositDto depositDto, Date date) {
		if (date == null) {
			depositDto.setDate(new Date());
		}
	}

	private void updateSolde(DepositDto depositDto) {
		Compte beneficiaire = compteRepository.findByRib(depositDto.getRib());
		beneficiaire.setSolde(beneficiaire.getSolde().add(depositDto.getMontant()));
	}

	private Deposit saveDeposit(DepositDto depositDto) {
		Compte beneficiaire = compteRepository.findByRib(depositDto.getRib());

		compteRepository.save(beneficiaire);

		Deposit deposit = new Deposit();

		deposit.setDateExecution(new Date());
		deposit.setMontant(depositDto.getMontant());
		deposit.setMotifDeposit(depositDto.getMotif());
		deposit.setCompteBeneficiaire(beneficiaire);
		deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());

		auditService.auditDeposit(depositDto);
		return depositRepository.save(deposit);
	}
}
