package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.audit.AuditTransfer;

public interface AuditTransferRepository extends JpaRepository<AuditTransfer, Long> {
}
