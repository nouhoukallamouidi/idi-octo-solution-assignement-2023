package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.model.Transfer;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
}
