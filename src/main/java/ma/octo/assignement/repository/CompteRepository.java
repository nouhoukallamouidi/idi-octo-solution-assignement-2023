package ma.octo.assignement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.model.Compte;



public interface CompteRepository extends JpaRepository<Compte, Long> {
  Compte findByNrCompte(String nrCompte);
  
  Compte findByRib(String rib);

  List<Compte> findComptesByUtilisateurId(Long utilisateurId);
}
