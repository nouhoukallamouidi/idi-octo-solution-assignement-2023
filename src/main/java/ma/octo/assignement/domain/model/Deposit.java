package ma.octo.assignement.domain.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "DEPOSIT")
@Getter
@Setter
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal montant;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;

    @Column
    private String nomPrenomEmetteur;

    @ManyToOne
    private Compte compteBeneficiaire;

    @Column(length = 200)
    private String motifDeposit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMontant() {
		return montant;
	}

	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}

	public Date getDateExecution() {
		return dateExecution;
	}

	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	public String getNomPrenomEmetteur() {
		return nomPrenomEmetteur;
	}

	public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
		this.nomPrenomEmetteur = nomPrenomEmetteur;
	}

	public Compte getCompteBeneficiaire() {
		return compteBeneficiaire;
	}

	public void setCompteBeneficiaire(Compte compteBeneficiaire) {
		this.compteBeneficiaire = compteBeneficiaire;
	}

	public String getMotifDeposit() {
		return motifDeposit;
	}

	public void setMotifDeposit(String motifDeposit) {
		this.motifDeposit = motifDeposit;
	}
    
}
