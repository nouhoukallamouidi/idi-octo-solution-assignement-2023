package ma.octo.assignement.dto.mapper;

import ma.octo.assignement.domain.model.Deposit;
import ma.octo.assignement.dto.model.DepositDto;

public class DepositMapper {
    private static DepositDto depositDto;

    public static DepositDto map(Deposit deposit) {
        depositDto = new DepositDto();
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDto.setMotif(deposit.getMotifDeposit());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setRib(deposit.getCompteBeneficiaire().getRib());

        return depositDto;
    }
}
