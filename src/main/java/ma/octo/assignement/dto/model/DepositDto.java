package ma.octo.assignement.dto.model;

import java.math.BigDecimal;
import java.util.Date;

public class DepositDto {
    private String rib;
    private BigDecimal montant;
    private Date date;
    private String nomPrenomEmetteur;
    private String cinEmetteur;
    private String motif;

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public String getCinEmetteur() {
        return cinEmetteur;
    }

    public void setCinEmetteur(String cinEmetteur) {
        this.cinEmetteur = cinEmetteur;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }
}
